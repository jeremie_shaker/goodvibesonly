var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var postsSchema = new Schema({
	title: String,
    name: String,
    artist: Array,
    remix: Array,
    genre: Array,
    url: String,
    description: String,
    blogger: String,
    date: { type: Date, default: Date.now }
});

mongoose.model('posts', postsSchema);