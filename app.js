
/**
 * Module dependencies
 */

var express = require('express'),
  routes = require('./routes'),
  api = require('./routes/api'),
  http = require('http'),
  path = require('path'),
  fs = require('fs'),
  mongoose = require('mongoose');

mongoose.connect("mongodb://admin:admin@dogen.mongohq.com:10061/EDMTL");
var models_path = __dirname + '/dbmodels'
fs.readdirSync(models_path).forEach(function (file) {
  require(models_path+'/'+file)
});


var app = module.exports = express();

/**
* Configuration
*/

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.static(path.join(__dirname, 'public')));
app.use(app.router);



// development only
if (app.get('env') === 'development') {
   app.use(express.errorHandler());
};

// production only
if (app.get('env') === 'production') {
  // TODO
}; 



// Routes
app.get('/', routes.index);
app.get('/partial/home', routes.home);
app.get('/partial/:name', routes.partial);
app.get('/mongoose', function(req, res){
	mongoose.model('posts').find(function(err, posts){
		res.send(posts);
	})
});

// JSON API
app.get('/api/name', api.name);
app.get('/api/songs', api.songs);

// redirect all others to the index (HTML5 history)
app.get('*', routes.index);

/**
* Start Server
*/

http.createServer(app).listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
});