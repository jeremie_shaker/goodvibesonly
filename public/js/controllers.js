'use strict';

/* Controllers */

function AppCtrl($scope, $http) {

    $http.get('/api/songs')
        .success(function(data, status, headers, config){
            $scope.posts = data;
            $scope.playlist = [];
            $scope.posts.forEach(function(post){
                $scope.playlist.push(post.url);
            });

        })
        .error(function(data, status, headers, config){
            $scope.posts= 'Error!';
            console.error(status);
        });


    $scope.init = function(){
        SC.initialize({
            client_id: '460a4501a3fbf17ee9de433ccac60f06' 
        });
        $scope.player = document.querySelector('iframe');
        $scope.widget = SC.Widget($scope.player);
        $scope.widget.bind(SC.Widget.Events.FINISH, function(){
            $scope.playlist.shift();
            $scope.load($scope.playlist[0]);
        });
    }

    $scope.load = function(url){
        console.log(url);
        $scope.widget.load(url, {auto_play: true});
    }
    
}

function HomeCtrl($scope, $http) {

}
HomeCtrl.$inject = ['$scope', '$http'];

function MyCtrl1() {}
MyCtrl1.$inject = [];


function MyCtrl2() {
}
MyCtrl2.$inject = [];

